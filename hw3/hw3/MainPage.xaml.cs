﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace hw3
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }
        async void Handle_NavigateToPage1(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());

        }
        async void Handle_NavigateToPage2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());

        }
        async void Handle_NavigateToPage3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());

        }
    }
   



}
